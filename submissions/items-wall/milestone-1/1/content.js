var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

// Module 'content'

//import { createRoot } from 'react-dom/client';

function decimals(x, decimals) {
    if (!x) return "";

    return x.toFixed(decimals);
}

function formatRating(x) {
    var value = decimals(x, 1);
    return value.endsWith('.0') ? decimals(x, 0) : value;
}

function Carousel(props) {
    var _React$useState = React.useState(0),
        _React$useState2 = _slicedToArray(_React$useState, 2),
        id = _React$useState2[0],
        setId = _React$useState2[1];

    if (props.item.images.length <= 0) return React.createElement(React.Fragment, null);else return React.createElement(
        'div',
        null,
        React.createElement(WallItemImage, { item: props.item, image: props.item.images[id] }),
        React.createElement(
            'div',
            { style: { display: 'flex', alignItems: 'center', justifyContent: 'center' } },
            props.item.images.map(function (img, imgId) {
                return React.createElement(
                    'span',
                    { key: img, className: 'gogo ' + (imgId == id ? 'gone' : ''), style: { fontSize: '25px', margin: '0 5px' }, onClick: function onClick(_) {
                            return setId(imgId);
                        } },
                    imgId == id ? String.fromCharCode(9675) : String.fromCharCode(9679)
                );
            })
        )
    );
}

function WallItemImage(props) {
    return React.createElement(
        'a',
        { href: props.item.url },
        React.createElement('img', { style: { "width": "100%" }, className: 'softened', src: "https://littlebiggy.org/" + props.image, alt: props.item.name })
    );
}

function BuyVariety(props) {
    if (props.variety.inCart) {
        return React.createElement(
            'div',
            { style: { "marginTop": "1%" } },
            React.createElement(
                'div',
                { className: 'leroy' },
                React.createElement(
                    'a',
                    { href: props.response.checkoutUrl, className: 'gogo Bp3' },
                    'buy'
                )
            ),
            React.createElement(
                'div',
                { className: 'reginald Bp3' },
                props.variety.qtyInCart,
                ' in cart'
            )
        );
    }

    return '';
}

function Variety(props) {
    return React.createElement(
        'div',
        { style: { "marginTop": "2%" }, className: 'js-variety' },
        React.createElement(
            'div',
            null,
            React.createElement(
                'div',
                { className: 'leroy', style: { "verticalAlign": "top" } },
                React.createElement(
                    'span',
                    { className: 'gogo Bp1' },
                    'buy'
                )
            ),
            React.createElement(
                'div',
                { className: 'reginald' },
                React.createElement(
                    'div',
                    { className: props.item.inCart ? 'gone' : '' },
                    React.createElement('span', { className: 'Bp1', dangerouslySetInnerHTML: { __html: props.variety.description } }),
                    '\xA0\xA0',
                    React.createElement(
                        'span',
                        { className: 'Bp1' },
                        React.createElement(
                            'span',
                            { className: 'price ' + props.variety.basePrice.currency },
                            React.createElement(
                                'span',
                                { className: 'currencySymbol' },
                                props.variety.basePrice.symbol
                            ),
                            props.variety.basePrice.amount
                        ),
                        props.variety.paymentPrices.map(function (paymentPrice) {
                            return React.createElement(
                                'span',
                                { key: paymentPrice.symbol + '_' + paymentPrice.amount, className: 'price ' + paymentPrice.currency },
                                '\xA0\xA0',
                                React.createElement(
                                    'span',
                                    { className: 'currencySymbol' },
                                    paymentPrice.symbol
                                ),
                                paymentPrice.amount
                            );
                        })
                    )
                )
            )
        ),
        React.createElement(BuyVariety, { response: props.response, variety: props.variety })
    );
}

function WallItem(props) {
    return React.createElement(
        'div',
        { className: 'wwItem js-item' },
        React.createElement(
            'div',
            null,
            React.createElement(
                'div',
                { className: 'leroy' },
                props.response.userRatings[props.item.id] ? React.createElement(
                    'div',
                    { className: 'Bp3' },
                    formatRating(props.response.userRatings[props.item.id]),
                    '/10',
                    React.createElement(
                        'span',
                        { className: 'mine' },
                        ':}'
                    )
                ) : ''
            ),
            React.createElement(
                'div',
                { className: 'reginald' },
                React.createElement(
                    'div',
                    null,
                    React.createElement('a', { href: props.item.url, className: "gogo Bp0 " + (props.item.inCart ? 'gomo' : ''), dangerouslySetInnerHTML: { __html: props.item.name } })
                ),
                React.createElement(
                    'div',
                    { className: 'Bp3', style: { "marginBottom": "1%" } },
                    props.response.itemReviewSummaries[props.item.id] ? React.createElement(
                        'a',
                        { href: '{props.url}?anchor=reviews' },
                        formatRating(props.response.itemReviewSummaries[props.item.id].averageRating),
                        ' ',
                        React.createElement(
                            'span',
                            { className: 'gogo' },
                            '/10'
                        )
                    ) : '',
                    props.response.itemReviewSummaries[props.item.id] ? React.createElement(
                        'span',
                        { style: { "marginBottom": "2%", "marginLeft": "2%" } },
                        props.response.itemReviewSummaries[props.item.id].numberOfReviews,
                        '\xA0',
                        React.createElement(
                            'a',
                            { href: props.item.url + "?anchor=reviews", className: 'gogo' },
                            props.response.itemReviewSummaries[props.item.id].numberOfReviews > 1 ? "reviews" : "review"
                        )
                    ) : ''
                )
            )
        ),
        React.createElement(
            'div',
            null,
            React.createElement('div', { className: 'leroy' }),
            React.createElement(
                'div',
                { className: 'reginald' },
                React.createElement(
                    'div',
                    null,
                    React.createElement(Carousel, { item: props.item })
                ),
                React.createElement('div', { className: 'Bp3', style: { "marginTop": "1%", "lineHeight": "120%" }, dangerouslySetInnerHTML: { __html: props.item.description } }),
                React.createElement(
                    'div',
                    { className: 'Bp1', style: { "marginTop": "3.5%", "lineHeight": "120%" } },
                    React.createElement(
                        'div',
                        { className: 'Bp1' },
                        'from ',
                        React.createElement(
                            'a',
                            { href: props.item.seller.url, className: 'gogo' },
                            props.item.seller.name
                        ),
                        props.response.sellerReviewSummaries[props.item.seller.id] ? React.createElement(
                            'a',
                            { href: props.item.seller.reviewsUrl, style: { "marginLeft": "2%" } },
                            formatRating(props.response.sellerReviewSummaries[props.item.seller.id].averageRating),
                            React.createElement(
                                'span',
                                { className: 'gogo' },
                                '/10'
                            )
                        ) : ''
                    ),
                    React.createElement(
                        'div',
                        { className: 'Bp1' },
                        'online ',
                        props.item.seller.online
                    ),
                    React.createElement(
                        'div',
                        { className: 'Bp3' },
                        'ships from ',
                        props.item.shipsFrom,
                        props.response.itemReviewSummaries[props.item.id] ? React.createElement(
                            'span',
                            null,
                            '\xA0(avg ',
                            decimals(props.response.itemReviewSummaries[props.item.id].averageDaysToArrive, 0),
                            ' ',
                            props.response.itemReviewSummaries[props.item.id].averageDaysToArrive > 1 ? "days" : "day",
                            ')'
                        ) : ''
                    )
                )
            )
        ),
        React.createElement(
            'div',
            { className: 'js-varieties' },
            props.item.varieties.map(function (x) {
                return React.createElement(Variety, { key: x.id, response: props.response, variety: x, item: props.item });
            })
        )
    );
}

function Wall(props) {
    return React.createElement(
        'div',
        { id: 'wonderwall' },
        React.createElement('div', { className: 'Bp2 gogo' }),
        React.createElement(
            'div',
            { style: { "marginBottom": "5%" }, className: 'Bp0' },
            React.createElement('div', { className: 'leroy' }),
            React.createElement(
                'div',
                { className: 'reginald' },
                React.createElement(
                    'div',
                    null,
                    props.response.numberOfItems,
                    ' ',
                    props.response.numberOfItems > 1 ? "items" : "item"
                ),
                React.createElement(
                    'div',
                    null,
                    props.response.numberOfSellers,
                    ' ',
                    props.response.numberOfSellers > 1 ? "sellers" : "seller"
                )
            )
        ),
        React.createElement(
            'div',
            { id: 'wonderwall-items' },
            props.response.items.map(function (x) {
                return React.createElement(WallItem, { key: x.id, item: x, response: props.response });
            })
        )
    );
}

function GetItems() {
    var url = "https://littlebiggy.org/api/json/wall/items?" + new URLSearchParams({ shipsTo: '' });
    //const url = "/items.json";

    return fetch(url, {
        method: 'GET',
        //mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
            'APIKey': '2ECE072BBCDB48B29D1EBEB60A832984'
        }
    }).then(function (x) {
        return x.json();
    });
}

function Loading() {
    return React.createElement(
        'div',
        null,
        'Loading...'
    );
}

function Content() {
    var _React$useState3 = React.useState(null),
        _React$useState4 = _slicedToArray(_React$useState3, 2),
        response = _React$useState4[0],
        setResponse = _React$useState4[1];

    var _React$useState5 = React.useState(null),
        _React$useState6 = _slicedToArray(_React$useState5, 2),
        error = _React$useState6[0],
        setError = _React$useState6[1];

    React.useEffect(function () {
        GetItems().then(function (x) {
            return setResponse(x);
        }).catch(function (x) {
            console.error(x);
            setError(x);
        });
    }, []);

    return React.createElement(
        React.StrictMode,
        null,
        React.createElement(
            'div',
            null,
            error === null ? '' : React.createElement('div', { className: 'errorContainer Bp3', style: { marginBottom: '3%' } }),
            response === null ? React.createElement(Loading, null) : React.createElement(Wall, { response: response })
        )
    );
}

var container = document.getElementById('content');
var root = ReactDOM.createRoot(container);
root.render(React.createElement(Content));